import os
import cv2
import numpy as np
from PIL import Image

def find_homography(left_image_array: str, right_image_array: str) -> tuple:
    """
    Finds the homography matrix between two images using SIFT feature matching and RANSAC algorithm.

    Args:
        left_image_array (str or numpy.ndarray): Path to the left image array file or a NumPy array representing the image.
        right_image_array (str or numpy.ndarray): Path to the right image array file or a NumPy array representing the image.

    Returns:
        tuple: A tuple containing the left crop dimensions, right crop dimensions,
               cropped left image, and cropped right image.

    Returns a tuple of the following elements:
        - left_crop_dimensions (tuple): The coordinates of the top-left and bottom-right corners of the cropping region
          for the left image. It specifies the rectangular region that should be cropped from the left image to remove
          any black borders introduced during the alignment process. The coordinates are in the format
          `[(x1, y1), (x2, y2)]`, where `(x1, y1)` represents the top-left corner and `(x2, y2)` represents the bottom-right
          corner of the cropping region.

        - right_crop_dimensions (tuple): The coordinates of the top-left and bottom-right corners of the cropping region
          for the right image. It specifies the rectangular region that should be cropped from the right image to match
          the cropped region of the left image. The coordinates are in the same format as `left_crop_dimensions`.

        - crop_img_left (numpy.ndarray): The cropped left image, which has been aligned with the right image using the
          homography matrix and cropped to remove any black borders. It is a NumPy array representing the image.

        - crop_img_right (numpy.ndarray): The cropped right image, which has been matched with the left image and cropped
          to match the region of the left image. It is a NumPy array representing the image.
    """
    # Check if the input left_image_array is a string or a NumPy array
    if isinstance(left_image_array, str):
        # Load the left image from disk
        left_image = np.load(left_image_array)
    elif isinstance(left_image_array, np.ndarray):
        # Use the input NumPy array as the left image
        left_image = left_image_array
    else:
        raise ValueError("Invalid input type for left_image_array. Expected str or numpy.ndarray.")

    # Check if the input right_image_array is a string or a NumPy array
    if isinstance(right_image_array, str):
        # Load the right image from disk
        right_image = np.load(right_image_array)
    elif isinstance(right_image_array, np.ndarray):
        # Use the input NumPy array as the right image
        right_image = right_image_array
    else:
        raise ValueError("Invalid input type for right_image_array. Expected str or numpy.ndarray.")

    left_image = np.squeeze(left_image, axis=2)
    right_image = np.squeeze(right_image, axis=2)

    # Convert images to PIL format and save as JPEGs
    left_image = Image.fromarray(left_image)
    left_image.save("left.jpeg")
    right_image = Image.fromarray(right_image)
    right_image.save("right.jpeg")

    # Load JPEGs into OpenCV and convert to grayscale
    left_image = cv2.imread("left.jpeg")
    left_image = cv2.cvtColor(left_image, cv2.COLOR_BGR2GRAY)
    os.remove("left.jpeg")
    right_image = cv2.imread("right.jpeg")
    right_image = cv2.cvtColor(right_image, cv2.COLOR_BGR2GRAY)
    os.remove("right.jpeg")

    # Apply SIFT feature matching
    sift = cv2.SIFT_create()
    kp1, des1 = sift.detectAndCompute(left_image, None)
    kp2, des2 = sift.detectAndCompute(right_image, None)

    # Filter good matches and compute homography matrix using RANSAC
    matcher = cv2.FlannBasedMatcher({'algorithm': 0, 'trees': 5}, {})
    matches = matcher.knnMatch(des1, des2, k=2)  # Find 2 best matches for each feature descriptor

    good_matches = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good_matches.append(m)  # Keep only the matches that are significantly better than the second best

    # Compute homography matrix using RANSAC algorithm if enough good matches are found
    if len(good_matches) > 10:
        # Convert the keypoints to a numpy array and reshape them to a (N,1,2) array
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        # Find the homography matrix and mask using RANSAC algorithm
        H, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        # Warp the left image to the right using the homography matrix and crop to remove black borders
        aligned_img = cv2.warpPerspective(left_image, H, (left_image.shape[1], left_image.shape[0]))
        _, thresh = cv2.threshold(aligned_img, 1, 255, cv2.THRESH_BINARY)
        contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        x, y, w, h = cv2.boundingRect(contours[0])

        left_crop_dimensions = [(x, y), (x + w, y + h)]
        right_crop_dimensions = [(0, 0), (x + w - x, y + h - y)]
        crop_img_right = right_image[left_crop_dimensions[0][1]:left_crop_dimensions[1][1], left_crop_dimensions[0][0]:left_crop_dimensions[1][0]]
        crop_img_left = left_image[right_crop_dimensions[0][1]:right_crop_dimensions[1][1], right_crop_dimensions[0][0]:right_crop_dimensions[1][0]]

        return left_crop_dimensions, right_crop_dimensions, crop_img_left, crop_img_right
    else:
        print("Not enough matches are found - %d/%d" % (len(good_matches), 10))  # Print a message if not enough good matches are found
        return None, None, None, None  # Return None


dim_left, dim_right, crop_img_left, crop_img_right = find_homography(
    './test_images//picture660.npy', 
    './test_images/picture800.npy',
    )

print(dim_left, dim_right)

combined_img = np.concatenate((crop_img_left, crop_img_right), axis=1)
cv2.namedWindow('Aligned Image', cv2.WINDOW_NORMAL)
cv2.imshow('Aligned Image', combined_img)
cv2.waitKey(0)
input("Press Enter to continue...")
cv2.destroyAllWindows()
