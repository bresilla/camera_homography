import cv2
import math
import numpy as np
import ids_peak.ids_peak as ids_peak
import ids_peak_ipl.ids_peak_ipl as ids_ipl
from homography import homography


right_image_crop_dim, left_image_crop_dim = None, None

ids_peak.Library.Initialize()
device_manager = ids_peak.DeviceManager.Instance()
device_manager.Update()
device_descriptors = device_manager.Devices()

print("Found Devices: " + str(len(device_descriptors)))
for device_descriptor in device_descriptors:
    print(device_descriptor.DisplayName())

### Camerasettings ###
ExposureTime = 40500 # in microseconds

### Initialize camera1, 660nm
device1 = device_descriptors[0].OpenDevice(ids_peak.DeviceAccessType_Exclusive)
print("Opened Device: " + device1.DisplayName())
remote_device_nodemap1 = device1.RemoteDevice().NodeMaps()[0]

remote_device_nodemap1.FindNode("TriggerSelector").SetCurrentEntry("ExposureStart")
remote_device_nodemap1.FindNode("TriggerSource").SetCurrentEntry("Software")
remote_device_nodemap1.FindNode("TriggerMode").SetCurrentEntry("On")
datastream1 = device1.DataStreams()[0].OpenDataStream()
payload_size1 = remote_device_nodemap1.FindNode("PayloadSize").Value()

for i in range(datastream1.NumBuffersAnnouncedMinRequired()):
    buffer = datastream1.AllocAndAnnounceBuffer(payload_size1)
    datastream1.QueueBuffer(buffer)
    
datastream1.StartAcquisition()
remote_device_nodemap1.FindNode("AcquisitionStart").Execute()
remote_device_nodemap1.FindNode("AcquisitionStart").WaitUntilDone()

remote_device_nodemap1.FindNode("ExposureTime").SetValue(ExposureTime)
remote_device_nodemap1.FindNode("ReverseX").SetValue(False)
remote_device_nodemap1.FindNode("ReverseY").SetValue(False)

### Initialize camera2, 800nm
device2 = device_descriptors[1].OpenDevice(ids_peak.DeviceAccessType_Exclusive)
print("Opened Device: " + device2.DisplayName())
remote_device_nodemap2 = device2.RemoteDevice().NodeMaps()[0]

remote_device_nodemap2.FindNode("TriggerSelector").SetCurrentEntry("ExposureStart")
remote_device_nodemap2.FindNode("TriggerSource").SetCurrentEntry("Software")
remote_device_nodemap2.FindNode("TriggerMode").SetCurrentEntry("On")
datastream2 = device2.DataStreams()[0].OpenDataStream()
payload_size2 = remote_device_nodemap2.FindNode("PayloadSize").Value()

for i in range(datastream2.NumBuffersAnnouncedMinRequired()):
    buffer = datastream2.AllocAndAnnounceBuffer(payload_size2)
    datastream2.QueueBuffer(buffer)
    
datastream2.StartAcquisition()
remote_device_nodemap2.FindNode("AcquisitionStart").Execute()
remote_device_nodemap2.FindNode("AcquisitionStart").WaitUntilDone()

remote_device_nodemap2.FindNode("ExposureTime").SetValue(ExposureTime)
remote_device_nodemap2.FindNode("ReverseX").SetValue(False)
remote_device_nodemap2.FindNode("ReverseY").SetValue(False)


while True:
    # Camera 1, 660nm
    remote_device_nodemap1.FindNode("TriggerSoftware").Execute()
    buffer1 = datastream1.WaitForFinishedBuffer(1000)
    raw_image_1 = ids_ipl.Image_CreateFromSizeAndBuffer(buffer1.PixelFormat(), buffer1.BasePtr(), buffer1.Size(), buffer1.Width(), buffer1.Height())
    datastream1.QueueBuffer(buffer1)
    picture660 = raw_image_1.get_numpy_3D()

    # Camera 2, 800nm
    remote_device_nodemap2.FindNode("TriggerSoftware").Execute()
    buffer2 = datastream2.WaitForFinishedBuffer(1000)
    raw_image_2 = ids_ipl.Image_CreateFromSizeAndBuffer(buffer2.PixelFormat(), buffer2.BasePtr(), buffer2.Size(), buffer2.Width(), buffer2.Height())
    datastream2.QueueBuffer(buffer2)
    picture800 = raw_image_2.get_numpy_3D()

    if right_image_crop_dim is None or left_image_crop_dim is None:
        left_image_crop_dim, right_image_crop_dim, *_ = homography(picture660, picture800)
        continue
    
    picture660 = picture660[left_image_crop_dim[0][1]:left_image_crop_dim[1][1], left_image_crop_dim[0][0]:left_image_crop_dim[1][0]]
    picture800 = picture800[right_image_crop_dim[0][1]:right_image_crop_dim[1][1], right_image_crop_dim[0][0]:right_image_crop_dim[1][0]]

    #Chlorophyll index (CI)
    CIfraction = picture800.astype(np.float32) / picture660.astype(np.float32) 
    pictureCI = CIfraction - 1